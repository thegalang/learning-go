package main

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Article struct {
	ID      string `json:"Id"`
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "golang_learning"
)

type ArticleRepository struct{}

var psqlInfo string = os.Getenv("DATABASE_URL")

func getDb() *gorm.DB {
	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)

	}
	return db
}

func (ArticleRepository) migrate() {
	db := getDb()
	defer db.Close()

	db.AutoMigrate(&Article{})
}

func (ArticleRepository) getAllArticles() []Article {

	db := getDb()
	defer db.Close()

	var Articles []Article
	db.Find(&Articles)
	return Articles

}

func (ArticleRepository) save(article Article) {

	db := getDb()
	defer db.Close()

	db.Save(&article)
}

func (ArticleRepository) deleteByID(id string) {

	db := getDb()
	defer db.Close()

	var article Article
	db.Where("ID = ?", id).Find(&article)
	db.Delete(&article)
}

func (ArticleRepository) findByID(id string) Article {

	db := getDb()
	defer db.Close()

	var article Article
	db.Where("ID = ?", id).Find(&article)
	return article

}
