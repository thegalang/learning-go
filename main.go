package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var ArticleRepo ArticleRepository

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func returnAllArticles(w http.ResponseWriter, r *http.Request) {

	articles := ArticleRepo.getAllArticles()
	fmt.Println("Endpoint hit: Return all articles")
	json.NewEncoder(w).Encode(articles)
}

func getArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	article := ArticleRepo.findByID(key)
	json.NewEncoder(w).Encode(article)
}

func createNewArticle(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	fmt.Fprintf(w, "%+v", string(reqBody))

	var article Article
	json.Unmarshal(reqBody, &article)

	ArticleRepo.save(article)

	json.NewEncoder(w).Encode(article)
}

func deleteArticle(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	key := vars["id"]

	ArticleRepo.deleteByID(key)
}

func updateArticle(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := ioutil.ReadAll(r.Body)
	var article Article
	json.Unmarshal(reqBody, &article)

	ArticleRepo.save(article)
}

func handleRequests() {

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/allArticles", returnAllArticles)
	myRouter.HandleFunc("/Article", createNewArticle).Methods("POST")
	myRouter.HandleFunc("/Article/{id}", getArticle)
	myRouter.HandleFunc("/deleteArticle/{id}", deleteArticle).Methods("DELETE")
	myRouter.HandleFunc("/updateArticle/{id}", updateArticle).Methods("PUT")

	log.Fatal(http.ListenAndServe(":8000", myRouter))
}

func main() {

	ArticleRepo.migrate()

	/*Articles = []Article{
		Article{ID: "1", Title: "Hello", Desc: "Article Description", Content: "Article Content"},
		Article{ID: "2", Title: "Hello 2", Desc: "Article Description", Content: "Article Content"},
	}

	for _, article := range Articles {
		ArticleRepo.save(article)
	}*/

	handleRequests()
}
